import os

import cv2
import scipy.misc
import numpy as np


def get_data(folder):
    X = []
    y = []

    for wbc_type in os.listdir(folder):
        print(f'Getting {wbc_type} images from {folder}')
        if not wbc_type.startswith('.'):
            for image_filename in os.listdir(folder + wbc_type):
                img_file = cv2.imread(folder + wbc_type + '/' + image_filename)
                img_file = scipy.misc.imresize(arr=img_file, size=(120, 160, 3))
                if img_file is not None:
                    img_arr = np.asarray(img_file)
                    X.append(img_arr)
                    y.append(wbc_type)
    X = np.asarray(X)
    y = np.asarray(y)
    return X, y
