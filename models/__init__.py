from tqdm import tqdm, tqdm_notebook

from simple import Net as SimpleNet


def fit(net, data_loader, criterion, optimizer, epochs, notebook=True, print_loss=100):
    """Train network on given dataset.

    Args:
        net: neural network model to train.
        data_loader: pytorch DataLoader to load training data.
        criterion: function to compute loss.
        optimizer: optimizer function (eg. SGD).
        epochs: number of times to pass through dataset.
        notebook: flag if function is used in jupyter notebook.
        print_loss: number of batches after which update printing loss.
    Returns:
        None
    """
    progress_bar = tqdm_notebook if notebook else tqdm

    for epoch in progress_bar(range(epochs)):
        running_loss = 0.0
        with progress_bar(data_loader, desc=f'Epoch {epoch + 1}') as progress:
            for i, data in enumerate(progress):
                inputs, labels = data

                optimizer.zero_grad()

                outputs = net(inputs)
                loss = criterion(outputs, labels)
                loss.backward()
                optimizer.step()

                running_loss += loss.item()
                if i % print_loss == print_loss - 1:
                    progress.postfix(loss=running_loss)
                    running_loss = 0.0


__all__ = [fit, SimpleNet]
